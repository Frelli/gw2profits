package com.gw2profits.api;

import java.lang.reflect.ParameterizedType;
import java.util.Collections;
import java.util.Map;

public interface ApiRequest<T> {

    String getUrl();

    @SuppressWarnings("unchecked")
    default Class<T> getResponseType() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    default Map<String, String> getQueryParams() {
        return Collections.emptyMap();
    }

}
