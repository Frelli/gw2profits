package com.gw2profits.api.commerce.response;

import lombok.Value;

@Value
public class Information {

    long unit_price;
    long quantity;
}
