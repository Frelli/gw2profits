package com.gw2profits.api.commerce.response;

import lombok.Value;

@Value
public class PriceResponse {

    long id;
    boolean whitelisted;
    Information buys;
    Information sells;
}
