package com.gw2profits.api.commerce;

import com.gw2profits.api.ApiRequest;
import com.gw2profits.api.commerce.response.PriceResponse;

import java.util.Collections;
import java.util.Map;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;

public final class CommercePricesApiRequestFactory {

    public static ApiRequest<Long[]> all() {
        return new ApiRequest<Long[]>() {
            @Override
            public String getUrl() {
                return "https://api.guildwars2.com/v2/commerce/prices";
            }

            @Override
            public Class<Long[]> getResponseType() {
                return Long[].class;
            }
        };
    }

    public static ApiRequest<PriceResponse> single(final long id) {
        return new ApiRequest<PriceResponse>() {
            @Override
            public String getUrl() {
                return "https://api.guildwars2.com/v2/commerce/prices/" + id;
            }

            @Override
            public Class<PriceResponse> getResponseType() {
                return PriceResponse.class;
            }
        };
    }

    public static ApiRequest<PriceResponse[]> multiple(long... ids) {
        return new ApiRequest<PriceResponse[]>() {
            @Override
            public String getUrl() {
                return "https://api.guildwars2.com/v2/commerce/prices";
            }

            @Override
            public Map<String, String> getQueryParams() {
                final String commaSeparatedIds = stream(ids).mapToObj(String::valueOf).collect(joining(","));
                return Collections.singletonMap("ids", commaSeparatedIds);
            }
        };
    }


}
