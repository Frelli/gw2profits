package com.gw2profits.api;

import akka.actor.ActorSystem;
import akka.http.javadsl.Http;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.HttpEntity;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.Query;
import akka.http.javadsl.model.Uri;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;

import java.util.concurrent.CompletableFuture;

public class ApiRequestExecutor {

    private final Http http;
    private final Materializer materializer;

    public ApiRequestExecutor(final ActorSystem actorSystem) {
        http = Http.get(actorSystem);
        materializer = ActorMaterializer.create(actorSystem);
    }

    public <T> CompletableFuture<T> execute(ApiRequest<T> request) {
        final Unmarshaller<HttpEntity, T> unmarshaller = Jackson.unmarshaller(request.getResponseType());
        final Uri uri = Uri.create(request.getUrl()).query(Query.create(request.getQueryParams()));

        return http.singleRequest(HttpRequest.create(uri.toString()), materializer)
                .thenCompose(httpResponse -> unmarshaller.unmarshal(httpResponse.entity(), materializer))
                .toCompletableFuture();
    }
}
