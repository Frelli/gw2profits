package com.gw2profits;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.gw2profits.infrastructure.Gw2ProfitsSupervisor;

public class Application {

    public static void main(String... args) throws Exception {
        final ActorSystem system = ActorSystem.create("gw2profits");

        final ActorRef supervisor = system.actorOf(Gw2ProfitsSupervisor.props(), "gw2profits-supervisor");
        supervisor.tell(new Gw2ProfitsSupervisor.Start(), ActorRef.noSender());

        try {
            System.out.println("Press ENTER to exit the system");
            System.in.read();
        } finally {
            system.terminate();
            Thread.sleep(5000);
        }
    }
}
