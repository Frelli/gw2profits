package com.gw2profits.infrastructure;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.gw2profits.ItemLoader;
import com.gw2profits.PriceHistory;
import com.gw2profits.api.ApiRequestExecutor;
import lombok.Value;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static akka.japi.pf.ReceiveBuilder.create;
import static akka.pattern.PatternsCS.ask;

public class Gw2ProfitsSupervisor extends AbstractActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    public static Props props() {
        return Props.create(Gw2ProfitsSupervisor.class);
    }

    final ApiRequestExecutor apiRequestExecutor;
    final ActorRef itemLoader;
    final Map<Long, ActorRef> priceHistories = new ConcurrentHashMap<>();

    private Gw2ProfitsSupervisor() {
        apiRequestExecutor = new ApiRequestExecutor(context().system());
        itemLoader = context().actorOf(ItemLoader.props(apiRequestExecutor), "item-loader");
    }

    @Override
    public void preStart() throws Exception {
        log.info("GW2 Profits Application Started.");
    }

    @Override
    public void postStop() throws Exception {
        log.info("GW2 Profits Application Stopped.");
    }

    @Override
    public Receive createReceive() {
        return create().match(Start.class, start -> {
            final ItemLoader.ItemsLoaded itemsLoaded = (ItemLoader.ItemsLoaded) ask(itemLoader, new ItemLoader.LoadItems(1L), 10000).toCompletableFuture().get();

            itemsLoaded.getLoadedItems().parallelStream().forEach(itemId -> {
                final ActorRef priceHistory = priceHistories.computeIfAbsent(itemId,
                        actorItemId -> context().actorOf(PriceHistory.props(apiRequestExecutor, actorItemId)));
                priceHistory.tell(new PriceHistory.RecordCurrentPrice(itemId), ActorRef.noSender());
            });
        }).matchAny(this::unhandled).build();
    }

    @Value
    public static class Start {
    }

}
