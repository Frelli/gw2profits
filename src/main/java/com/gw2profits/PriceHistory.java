package com.gw2profits;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import com.gw2profits.api.ApiRequestExecutor;
import com.gw2profits.api.commerce.response.Information;
import com.gw2profits.api.commerce.response.PriceResponse;
import lombok.Value;

import java.time.Instant;
import java.util.Set;

import static akka.pattern.PatternsCS.pipe;
import static com.gw2profits.api.commerce.CommercePricesApiRequestFactory.single;
import static java.util.Collections.emptySet;

public class PriceHistory extends AbstractActor {

    private final LoggingAdapter log = Logging.getLogger(context().system(), this);

    private final ApiRequestExecutor apiRequestExecutor;

    private final long itemId;
    private final Set<PriceHistoryEntry> priceHistory = emptySet();

    public static Props props(final ApiRequestExecutor apiRequestExecutor, long itemId) {
        return Props.create(PriceHistory.class, apiRequestExecutor, itemId);
    }

    public PriceHistory(final ApiRequestExecutor apiRequestExecutor, long itemId) {
        this.apiRequestExecutor = apiRequestExecutor;
        this.itemId = itemId;
    }

    @Override
    public void preStart() throws Exception {
        log.debug("PriceHistory actor {} started.", itemId);
    }

    @Override
    public void postStop() throws Exception {
        log.debug("PriceHistory actor {} stopped.", itemId);
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create().match(RecordCurrentPrice.class, recordCurrentPrice -> {
            final ActorRef sender = getSender();
            sendPriceRecordedResponse(sender);
        }).build();
    }

    private void sendPriceRecordedResponse(ActorRef sender) {
        pipe(apiRequestExecutor.execute(single(itemId)).thenApply(this::recordPriceHistory), context().dispatcher()).to(sender);
    }

    private PriceHistoryEntry recordPriceHistory(PriceResponse priceResponse) {
        final PriceHistoryEntry priceHistoryEntry =
                new PriceHistoryEntry(priceResponse.getId(),
                        priceResponse.getBuys(),
                        priceResponse.getSells(),
                        Instant.now().toEpochMilli());
        priceHistory.add(priceHistoryEntry);
        return priceHistoryEntry;
    }

    @Value
    public static final class PriceHistoryEntry {
        long id;
        Information buys;
        Information sells;
        long timestamp;
    }

    @Value
    public static final class RecordCurrentPrice {
        long requestId;
    }

    public static final class ReadPriceHistory {
        long requestId;
        long itemId;
        Set<PriceHistoryEntry> priceHistoryEntries;

    }
}
