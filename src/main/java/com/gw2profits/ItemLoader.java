package com.gw2profits;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import com.gw2profits.api.ApiRequestExecutor;
import com.gw2profits.api.commerce.CommercePricesApiRequestFactory;
import lombok.Value;

import java.util.Arrays;
import java.util.Collection;

import static akka.pattern.PatternsCS.pipe;

public class ItemLoader extends AbstractActor {

    private final LoggingAdapter log = Logging.getLogger(context().system(), this);

    private final ApiRequestExecutor apiRequestExecutor;

    public static Props props(final ApiRequestExecutor apiRequestExecutor) {
        return Props.create(ItemLoader.class, apiRequestExecutor);
    }

    public ItemLoader(ApiRequestExecutor apiRequestExecutor) {
        this.apiRequestExecutor = apiRequestExecutor;
    }

    @Override
    public Receive createReceive() {
        return ReceiveBuilder.create().match(LoadItems.class, loadItems -> {
                    final ActorRef sender = getSender();
                    pipe(apiRequestExecutor.execute(CommercePricesApiRequestFactory.all())
                            .thenApply(Arrays::asList)
                            .thenApply(loadedItems -> new ItemsLoaded(loadItems.requestId, loadedItems)), context().dispatcher())
                            .to(sender);
                }
        ).build();
    }

    @Value
    public static class LoadItems {
        long requestId;
    }

    @Value
    public static class ItemsLoaded {
        long requestId;
        Collection<Long> loadedItems;
    }
}
